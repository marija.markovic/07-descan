#ifndef DRIVE_H
#define DRIVE_H

#include <QAbstractOAuth>
#include <QByteArray>
#include <QDesktopServices>
#include <QFile>
#include <QFileInfo>
#include <QHttpMultiPart>
#include <QHttpPart>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>
#include <QOAuth2AuthorizationCodeFlow>
#include <QOAuthHttpServerReplyHandler>
#include <QObject>
#include <QString>
#include <QVariant>

class Drive : public QObject {
  Q_OBJECT

public:
  Drive();
  ~Drive();

  void uploadToDrive(const QStringList &filePaths);

public slots:
  void saveTokenAndConnect();
  void postRequest();
  void putRequest();

private:
  QOAuth2AuthorizationCodeFlow *google;
  QOAuthHttpServerReplyHandler *replyHandler;
  QNetworkAccessManager *manager;
  QNetworkReply *reply;
  QFile *tokenFile;
  QStringList filePaths;
  QStringList::iterator currentFile;

signals:
  void endConnectSignal();
  void fileUploadedSignal();
};

#endif // DRIVE_H
